
(function($){
    
   "use strict" ;
    
    $(document).ready(function(){
              
        $(".search-icon").click(function(){
            
            $(".search-form").toggle("slow");
            
        });
        
     // carosel 
     $('.carousel').carousel({
     interval: 5000
       });
        
        // counter 
        
        $('#myStat-1').circliful();
         $('#myStat-2').circliful();
          $('#myStat-3').circliful();
           $('#myStat-4').circliful();
           
            // Sticky Menu
    $(".main_header").sticky({
        topSpacing: 0
    });
       
	    // The actual plugin
            $('.nav').singlePageNav({
                offset: $('.nav').outerHeight(),
                filter: ':not(.external)',
                updateHash: true,
                beforeStart: function() {
                    console.log('begin scrolling');
                },
                onComplete: function() {
                    console.log('done scrolling');
                }
            });
            
            // click to scroll up
	
		$('.scrollup').click(function(){
			$("html,body").animate({
				scrollTop:0
			},600);
			return false;
		});
			$(window).on('scroll',function(){
				
				if($(this).scrollTop() >250){
					$('.scrollup').fadeIn();
				}else{
					$('.scrollup').fadeOut();
				}
				
			});

                        // Google map 
                        map = new GMaps({
                        el: '#map',
                        lat:  23.739296,
                        lng:  90.389254
                      });
                      map.addMarker({
                        lat: 23.739296,
                        lng: 90.389254,
                        title: 'Webcode',
                        details: {
                          database_id: 42,
                          author: 'Sajjad'
                        },
                        click: function(e){
                          if(console.log)
                            console.log(e);
                          alert('Webcode BD');
                        },
                        mouseover: function(e){
                          if(console.log)
                            console.log(e);
                        }
                      });
                      map.addMarker({
                        lat:23.739296,
                        lng: 90.389254,
                        title: 'Webcode BD Institute',
                        infoWindow: {
                          content: '<h2>Webcode BD</h2><p> Dhaka </p>'
                        }
                      });

       
  $('.test-popup-link').magnificPopup({
  type: 'video'
  // other options
});

          
    });
    
    
})(jQuery);
